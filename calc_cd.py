from pathlib import Path
from sys import argv
from numeric.analysis import *
from math import pi
import matplotlib.pyplot as pl
from scipy.signal import savgol_filter


def key_func(a):
    return int(str(a).split("/")[-1].split(".")[0])


def area(diameter):
    return pi * (diameter / 2)**2

ma = {
    1: (0.0033, 0.035),
    2: (0.0033, 0.035),
    3: (0.0025, 0.035),
    4: (0.0025, 0.035),
    5: (0.0012, 0.035),
    6: (0.0012, 0.035),
    7: (0.0055, 0.070),
    8: (0.0055, 0.070),
    9: (0.0035, 0.070),
    10: (0.0035, 0.070),
    11: (0.0015, 0.070),
    12: (0.0015, 0.070)
}

g = 9.81
rho = 1.2

work_dir = Path(argv[1])
files = sorted(list(work_dir.iterdir()), key=key_func)
for filename in files:
    print("-" * 20, "\n", filename)
    m = ma[key_func(filename)][0]
    a = area(ma[key_func(filename)][1])
    values = read_file_data(filename)
    filtered = savgol_filter(values.T[1], len(values) - (len(values) + 1) % 2, 3)
    x_smooth = np.zeros((len(filtered), 2))
    x_smooth.T[0] = values.T[0]
    x_smooth.T[1] = filtered
    v_smooth = diff_array2(x_smooth, 0, 1)
    pl.plot(values.T[0], values.T[1], values.T[0], x_smooth.T[1])
    pl.show()
    pl.plot(v_smooth.T[0], v_smooth.T[1])
    pl.show()
    cd = calc_drag_coefficient(values, m, g, rho, a)
    print("Cd =", cd)

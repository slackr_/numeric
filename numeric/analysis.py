import numpy as np
import matplotlib.pyplot as pl
from scipy.signal import savgol_filter


def read_file_data(filename):
    """ Read measured data from file filename """
    try:
        values = np.genfromtxt(filename, dtype=np.float128, delimiter=' ', skip_header=2)
        translate_array(values, 1, -values[0][1])
    except FileNotFoundError:
        return None
    return values


def translate_array(array, index, trans_val):
    for i, val in enumerate(array.T[index]):
        array.T[index][i] = val + trans_val


def diff_array(array, t_index, pos_index):
    """
        Numerically differentiate the function f
        where f(array[i][t_index]) = array[i][pos_index]
        using Newton's difference quotient
    """
    v = np.zeros((len(array) - 1, 2), dtype=np.float128)
    for i in range(1, len(array)):
        v[i - 1][0] = array[i - 1][t_index]
        v[i - 1][1] = (array[i][pos_index] - array[i - 1][pos_index]) / \
            (array[i][t_index] - array[i - 1][t_index])
    return v


def diff_array2(array, t_index, pos_index):
    """
        Numerically differentiate the function f
        where f(array[i][t_index]) = array[i][pos_index]
        using the symmetric difference quotient
    """

    v = np.zeros((len(array) - 2, 2), dtype=np.float128)
    for i in range(1, len(array) - 1):
        v[i - 1][0] = array[i - 1][t_index]
        v[i - 1][1] = (array[i + 1][pos_index] - array[i - 1][pos_index]) / \
                      (array[i + 1][t_index] - array[i - 1][t_index])
    return v


def euler_double(x_0, v_0, t_0, h, n, m, g, k, q):
    """
        Numerically integrate dv/dt twice with initial values
        x(t_0) = x_0, v(t_0) = v_0, t_0
        using step size h and n iterations
        to obtain position and velocity
    """
    values = np.zeros((n, 3), dtype=np.float128)
    t = np.float128(t_0)
    v = np.float128(v_0)
    x = np.float128(x_0)
    for i in range(1, n + 1):
        t += h
        x += v * h
        v += h * ((k * v + q * v**2) / m - g)
        values[i - 1][0] = t
        values[i - 1][1] = v
        values[i - 1][2] = x
    return values


def get_smooth_vel(values, t_index, pos_index):
    v = diff_array2(values, t_index, pos_index)
    filtered = savgol_filter(v.T[1], len(v) - (len(v) + 1) % 2, 3)
    v.T[1] = filtered
    return v


def find_terminal_vel(values, t_index, pos_index):
    """ Find the terminal velocity of the object """
    filtered = savgol_filter(values.T[pos_index], len(values) - (len(values) + 1) % 2, 3)
    x_smooth = np.zeros((len(filtered), 2))
    x_smooth.T[0] = values.T[t_index]
    x_smooth.T[1] = filtered
    v = diff_array2(x_smooth, 0, 1)
    return min(v.T[1])


def circle_area(diameter):
    return np.pi * (diameter / 2)**2


def calc_drag_coefficient(values, m, g, rho, a):
    """ Calculate the drag coefficient for the object """
    v = find_terminal_vel(values, 0, 1)
    return (2 * m * abs(g)) / (rho * a * v**2)

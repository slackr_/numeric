import numpy as np
import matplotlib.pylab as plt

g = 9.81  # gravitasjonskonstant
m = 0.000485  # massen til formen i kg
v_T = -1.42 # terminalhastighet i meter/sekund
rho = 1.17  # lufttettheten
A = 2*np.pi*(0.5*0.085)**2  # areal av formen
k = 0.0000001  # luftmotstandskoeffisient for terminalhastighet
q = (m*g)/(v_T**2)# luftmotstandskoeffisient for turbulent (v**2) komponent
C_D = 2*q/(rho*A)


N = 750  # antall steg
h = 0.001  # steglengde

# initialverdier:
x_0 = 1.0  # startverdi til x i centimeter
v_0 = 0.0  # startverdi til v i meter/sekund
t_0 = 0.0  # start i sekunder

t = np.zeros(N+1)
x = np.zeros(N+1)
v = np.zeros(N+1)
t[0] = t_0
x[0] = x_0
v[0] = v_0

# N2: x'' = (k*x' + q*(x')**2)/m - g =>
# 		v' = (k*v + q*v**2)/m - g
# 		x' = v

for n in range(N):  # Eulers metode
    x_new = x[n] + h*v[n]
    v_new = v[n] + h*((k*v[n] + q*v[n]**2)/m - g)
    t[n+1] = t[n] + h
    x[n+1] = x_new
    v[n+1] = v_new

plt.figure()
plt.plot(t,x, 'b')
plt.xlabel(r'$t$')
plt.ylabel(r'$x(t)$')
plt.title('Posisjonsplott av muffinsform i fritt fall.')
plt.grid()


'''
plt.figure()
plt.plot(t,v)
plt.xlabel(r'$t$')
plt.ylabel(r'$v(t)$')
plt.title('Fartsplott av muffinsform i fritt fall.')
plt.grid()
plt.show()
'''

data1 = np.loadtxt('data3mass1.txt')
t_exp = data1[:,0]
x_exp = data1[:,1]*10**(-2)
plt.plot(t_exp, x_exp, 'r.')

plt.show()
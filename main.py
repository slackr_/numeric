import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

import numpy as np
from numpy import float128
import matplotlib.pyplot as pl

from numeric.analysis import *


class MainWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Numerical Integration")
        self.root_box = Gtk.Box(spacing=60, orientation=Gtk.Orientation.HORIZONTAL)
        self.add(self.root_box)
        file_button = Gtk.Button(label="Choose Input File")
        file_button.connect("clicked", self.on_file_button_clicked)

        file_info_label = Gtk.Label("File chosen:")
        self.file_chosen_text = Gtk.Label("None")

        file_info_box = Gtk.Box(spacing=6, orientation=Gtk.Orientation.HORIZONTAL)
        file_info_box.pack_start(file_info_label, True, True, 4)
        file_info_box.pack_start(self.file_chosen_text, True, True, 4)

        self.gen_plot_button = Gtk.Button(label="Generate Numerical Plot")
        self.gen_plot_button.connect("clicked", self.on_gen_plot_button_clicked)
        self.gen_plot_button.set_sensitive(False)

        self.calculate_cd_button = Gtk.Button(label="Calculate Drag Coefficient")
        self.calculate_cd_button.connect("clicked", self.on_calculate_cd_button_clicked)
        self.calculate_cd_button.set_sensitive(False)

        file_box = Gtk.Box(spacing=6, orientation=Gtk.Orientation.VERTICAL)
        file_box.pack_start(file_button, True, True, 4)
        file_box.pack_start(file_info_box, False, True, 4)
        file_box.pack_start(self.calculate_cd_button, True, True, 4)
        file_box.pack_start(self.gen_plot_button, True, True, 4)

        mass_info_label = Gtk.Label("m:")
        mass_unit_label = Gtk.Label("kg")
        self.mass_entry = Gtk.Entry()
        self.mass_entry.set_text('0.0033')
        mass_box = Gtk.Box(spacing=6, orientation=Gtk.Orientation.HORIZONTAL)
        mass_box.pack_start(mass_info_label, True, True, 4)
        mass_box.pack_start(self.mass_entry, True, True, 2)
        mass_box.pack_start(mass_unit_label, True, True, 4)

        g_info_label = Gtk.Label("g:")
        g_unit_label = Gtk.Label("m/s^2")
        self.g_entry = Gtk.Entry()
        self.g_entry.set_text('9.81')
        g_box = Gtk.Box(spacing=6, orientation=Gtk.Orientation.HORIZONTAL)
        g_box.pack_start(g_info_label, True, True, 4)
        g_box.pack_start(self.g_entry, True, True, 2)
        g_box.pack_start(g_unit_label, True, True, 4)

        q_info_label = Gtk.Label("q:")
        self.q_entry = Gtk.Entry()
        self.q_entry.set_text('1.0')
        q_box = Gtk.Box(spacing=6, orientation=Gtk.Orientation.HORIZONTAL)
        q_box.pack_start(q_info_label, True, True, 4)
        q_box.pack_start(self.q_entry, True, True, 4)

        k_info_label = Gtk.Label("k:")
        self.k_entry = Gtk.Entry()
        self.k_entry.set_text('0.0032')
        # self.k_adj = Gtk.Adjustment(value=1.0, lower=-20.0, upper=20.0, step_incr=0.1)
        # k_slider = Gtk.HScale(adjustment=self.k_adj)
        # self.k_entry.connect('changed', self.on_k_entry_changed)
        # self.k_adj.connect('value-changed', self.on_k_slider_changed)

        k_entry_box = Gtk.Box(spacing=6, orientation=Gtk.Orientation.VERTICAL)
        # k_entry_box.pack_start(k_slider, True, True, 4)
        k_entry_box.pack_start(self.k_entry, True, True, 4)

        k_box = Gtk.Box(spacing=6, orientation=Gtk.Orientation.HORIZONTAL)
        k_box.pack_start(k_info_label, True, True, 4)
        k_box.pack_start(k_entry_box, True, True, 4)

        value_box = Gtk.Box(spacing=6, orientation=Gtk.Orientation.VERTICAL)
        value_box.pack_start(mass_box, False, True, 4)
        value_box.pack_start(g_box, False, True, 4)
        value_box.pack_start(q_box, False, True, 4)
        value_box.pack_start(k_box, False, True, 4)

        value_separator = Gtk.HSeparator()

        rho_info_label = Gtk.Label("rho:")
        self.rho_entry = Gtk.Entry()
        self.rho_entry.set_text("1.2")
        rho_unit_label = Gtk.Label("kg/m^3")
        rho_box = Gtk.Box(spacing=6, orientation=Gtk.Orientation.HORIZONTAL)
        rho_box.pack_start(rho_info_label, True, True, 4)
        rho_box.pack_start(self.rho_entry, True, True, 4)
        rho_box.pack_start(rho_unit_label, True, True, 4)

        d_info_label = Gtk.Label("D:")
        self.d_entry = Gtk.Entry()
        self.d_entry.set_text("0.035")
        d = Gtk.Label("m")
        d_box = Gtk.Box(spacing=6, orientation=Gtk.Orientation.HORIZONTAL)
        d_box.pack_start(d_info_label, True, True, 4)
        d_box.pack_start(self.d_entry, True, True, 4)
        d_box.pack_start(d, True, True, 4)

        value_box.pack_start(value_separator, True, True, 4)
        value_box.pack_start(rho_box, True, True, 4)
        value_box.pack_start(d_box, True, True, 4)

        self.root_box.pack_start(value_box, False, True, 4)
        self.root_box.pack_start(file_box, True, True, 4)

        self.read_data = None
        self.numeric_data = None
        self.datafile_name = ""

    def on_file_button_clicked(self, button):
        file_picker = Gtk.FileChooserDialog(
            title="Choose an input file",
            action=Gtk.FileChooserAction.OPEN,
            buttons=(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                     Gtk.STOCK_OPEN, Gtk.ResponseType.OK)
        )
        response = file_picker.run()
        if response == Gtk.ResponseType.OK:
            self.datafile_name = file_picker.get_filename()
        elif response == Gtk.ResponseType.CANCEL:
            pass
        file_picker.destroy()
        if is_valid_datafile(self.datafile_name):
            self.file_chosen_text.set_text(self.datafile_name)
            self.calculate_cd_button.set_sensitive(True)
            self.gen_plot_button.set_sensitive(True)

    def on_calculate_cd_button_clicked(self, button):
        self.read_data = read_file_data(self.datafile_name)
        self.cd = calc_drag_coefficient(
            self.read_data,
            float128(self.mass_entry.get_text()),
            float128(self.g_entry.get_text()),
            float128(self.rho_entry.get_text()),
            circle_area(float128(self.d_entry.get_text()))
        )
        print("Calculated coefficient:", self.cd)
        self.q_entry.set_text(
            str(0.5 * self.cd * float128(self.rho_entry.get_text())
                * circle_area(float128(self.d_entry.get_text())))
        )

    def on_gen_plot_button_clicked(self, button):
        self.read_data = read_file_data(self.datafile_name)
        smooth_vel = get_smooth_vel(self.read_data, 0, 1)
        self.numeric_data = euler_double(
            x_0=self.read_data[0][1],
            v_0=smooth_vel[0][1],
            t_0=self.read_data[0][0],
            h=(self.read_data[-1][0] - self.read_data[0][0]) / 2000,
            n=2000,
            m=float128(self.mass_entry.get_text()),
            g=float128(self.g_entry.get_text()),
            k=float128(self.k_entry.get_text()),
            q=float128(self.q_entry.get_text()),
        )
        # HACK for now
        # find_terminal_vel(self.read_data, 0, 1)
        # pl.plot(self.numeric_data.T[0], self.numeric_data.T[1])
        pl.plot(
            self.read_data.T[0], self.read_data.T[1], 'C1-',
            self.numeric_data.T[0], self.numeric_data.T[2], 'k--'
        )
        pl.title('Posisjon til form i fritt fall')
        pl.xlabel('t (s)')
        pl.ylabel('y (m)')
        pl.grid(True)
        y_min, y_max = pl.gca().get_ylim()
        y_mid = (y_min + y_max) / 2
        y_lower = (y_mid + y_min) / 2
        pl.text(0, y_mid,
                'm = {} kg\nA = {:.2g} m^2\nk = {:.4g}\nq = {:.4g}\n(Cd = {:.4g})'.format(
                    float(self.mass_entry.get_text()),
                    float(circle_area(float(self.d_entry.get_text()))),
                    float(self.k_entry.get_text()),
                    float(self.q_entry.get_text()),
                    float(self.cd)
                )
        )
        pl.text(0, y_lower, 'Svart: Numerisk løsning\nOransje: Eksperimentelt data')
        pl.show()

    def on_k_entry_changed(self, widget):
        try:
            value = float(self.k_entry.get_text())
        except ValueError:
            return
        self.k_adj.set_value(value)

    def on_k_slider_changed(self, widget):
        self.k_entry.set_text(str(self.k_adj.get_value()))


def is_valid_datafile(filename):
    if type(filename) != str:
        return False
    split = filename.split(".")
    return len(split) >= 2 and split[-1].lower() == "txt"


win = MainWindow()
win.connect("delete-event", Gtk.main_quit)
win.resize(1600, 300)
win.show_all()
Gtk.main()

